
// useful libs
var http = require("http");
var fs = require("fs");
var websocket = require("websocket").server;
var path = require('path');

// general variables
var port = 1234;
var webrtc_clients = [];
var webrtc_discussions = {};

// web server functions
var http_server = http.createServer(function(request, response) {
    //response.end(page);
    console.log('request starting...');

   var filePath = '.' + request.url;
   if (filePath == './')
       filePath = './index.html';

   var extname = path.extname(filePath);
   var contentType = 'text/html';
   switch (extname) {
       case '.js':
           contentType = 'text/javascript';
           break;
       case '.css':
           contentType = 'text/css';
           break;
       case '.json':
           contentType = 'application/json';
           break;
       case '.png':
           contentType = 'image/png';
           break;
       case '.jpg':
           contentType = 'image/jpg';
           break;
       case '.wav':
           contentType = 'audio/wav';
           break;
   }

   fs.readFile(filePath, function(error, content) {
       if (error) {
           if(error.code == 'ENOENT'){
               fs.readFile('./404.html', function(error, content) {
                   response.writeHead(200, { 'Content-Type': contentType });
                   response.end(content, 'utf-8');
               });
           }
           else {
               response.writeHead(500);
               response.end('Sorry, check with the site admin for error: '+error.code+' ..\n');
               response.end();
           }
       }
       else {
           response.writeHead(200, { 'Content-Type': contentType });
           response.end(content, 'utf-8');
       }
   });

});
http_server.listen(port, function() {
  log_comment("server listening (port "+port+")");
});
var page = undefined;
fs.readFile("index.html", function(error, data) {
  if (error) {
    log_error(error);
  } else {
    page = data;
  }
});

// web socket functions
var websocket_server = new websocket({
  httpServer: http_server
});
websocket_server.on("request", function(request) {
  log_comment("new request ("+request.origin+")");

  var connection = request.accept(null, request.origin);
  log_comment("new connection ("+connection.remoteAddress+")");

  webrtc_clients.push(connection);
  connection.id = webrtc_clients.length-1;

  connection.on("message", function(message) {
    if (message.type === "utf8") {
      log_comment("got message "+message.utf8Data);

      var signal = undefined;
      try { signal = JSON.parse(message.utf8Data); } catch(e) { };
      if (signal) {
        if (signal.type === "join" && signal.token !== undefined) {
          try {
            if (webrtc_discussions[signal.token] === undefined) {
              webrtc_discussions[signal.token] = {};
            }
          } catch(e) { };
          try {
            webrtc_discussions[signal.token][connection.id] = true;
          } catch(e) { };
        } else if (signal.token !== undefined) {
          try {
            Object.keys(webrtc_discussions[signal.token]).forEach(function(id) {
              if (id != connection.id) {
                webrtc_clients[id].send(message.utf8Data, log_error);
              }
            });
          } catch(e) { };
        } else {
          log_comment("invalid signal: "+message.utf8Data);
        }
      } else {
        log_comment("invalid signal: "+message.utf8Data);
      }
    }
  });

  connection.on("close", function(reason_code, description) {
    log_comment("connection closed ("+this.remoteAddress+" - "+reason_code+" - "+description+")");
    Object.keys(webrtc_discussions).forEach(function(token) {
      Object.keys(webrtc_discussions[token]).forEach(function(id) {
        if (id === connection.id) {
          delete webrtc_discussions[token][id];
        }
      });
    });
  });
});

// utility functions
function log_error(error) {
  if (error !== "Connection closed" && error !== undefined) {
    log_comment("ERROR: "+error);
  }
}
function log_comment(comment) {
  console.log((new Date())+" "+comment);
}
