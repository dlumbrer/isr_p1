var call_token; // unique token for this call
var signaling_server; // signaling server for this call
var peer_connection; // peer connection object
var file_store = []; // shared file storage
var local_stream_added = false;


var receiveBuffer = []
var receivedSize = 0;
var fileReceived = {};


function start() {
  // create the WebRTC peer connection object
  peer_connection = new rtc_peer_connection({ // RTCPeerConnection configuration
    "iceServers": [ // information about ice servers
      { "url": "stun:"+stun_server }, // stun server info
    ]
  });

  ///////////////////////////APARTADO 1 - SOPORTE DE TEXTO////////////////////////
  peer_connection.ondatachannel = function(event){
    receiveChannel = event.channel;
    receiveChannel.onmessage = function(event){

      /////////////////CHAT/////////////////
      if(typeof event.data == "string"){
        document.querySelector("#chatAreaReceive").innerHTML += event.data + "\n"
        return
      }

      /////////////////FICHERO/////////////
      var downloadAnchor = document.querySelector('a#download');
      receiveBuffer.push(event.data);
      receivedSize += event.data.byteLength;

      if (receivedSize === fileReceived.size) {
       var received = new window.Blob(receiveBuffer);
       receiveBuffer = [];
       downloadAnchor.textContent = ""
       downloadAnchor.href = URL.createObjectURL(received);
       downloadAnchor.download = fileReceived.name;
       downloadAnchor.textContent =
         'Click to download \'' + fileReceived.name + '\' (' + fileReceived.size + ' bytes)';
       downloadAnchor.style.display = 'block';
       receivedSize = 0;
     }else{
       downloadAnchor.textContent = 'Receiving... \'' + fileReceived.name + '\' (' + receivedSize + '/' + fileReceived.size + ' bytes received)';
       downloadAnchor.style.display = 'block';
     }
     console.log(receivedSize)
     console.log(fileReceived.size)

    }
  }

  sendChannel = peer_connection.createDataChannel("sendDataChannel", {reliable: false});

  document.querySelector("#sendChatButton").onclick = function(){
    var data = document.querySelector("#chatAreaSend").value;
    document.querySelector("#chatAreaSend").value = "";
    document.querySelector("#chatAreaReceive").innerHTML += "Me -> " + data + "\n";
    sendChannel.send(data);
  }
  ////////////////////////////////////////////////////////////////////////////

  /////////////////////////////////APARTADO 2 - FICHEROS///////////////////
  document.querySelector("#sendFileButton").onclick = function(){
    var fileraw = document.getElementById("fileToSend");
    var file = fileraw.files[0];
    if(!file){
      alert("Primero seleccione un fichero")
      return
    }
    console.log('File is ' + [file.name, file.size, file.type,
        file.lastModifiedDate
    ].join(' '));

    signaling_server.send(
      JSON.stringify({
        token:call_token,
        type:"file_signal",
        fname: file.name,
        fsize: file.size,
        ftype: file.type
      })
    );


    if (file.size === 0) {
      alert("Empty file, try again")
      return
    }
    ////////////PARTIR EL FICHERO EN CHUNKS///////////////////////
    var chunkSize = 8000; //testear con este valor para ver los mensajes
    //var chunkSize = 16384;
    var sliceFile = function(offset) {
      var reader = new window.FileReader();
      reader.onload = (function() {
        return function(e) {
          sendChannel.send(e.target.result);
          if (file.size > offset + e.target.result.byteLength) {
            window.setTimeout(sliceFile, 0, offset + chunkSize);
          }
        };
      })(file);
      var slice = file.slice(offset, offset + chunkSize);
      reader.readAsArrayBuffer(slice);
    };
    sliceFile(0);
  }
  /////////////////////////////////////////////////////////////////////////////

  // generic handler that sends any ice candidates to the other peer
  peer_connection.onicecandidate = function (ice_event) {
    console.log("new ice candidate");
    if (ice_event.candidate) {
      signaling_server.send(
        JSON.stringify({
          token:call_token,
          type: "new_ice_candidate",
          candidate: ice_event.candidate ,
        })
      );
    }
  };

  // display remote video streams when they arrive using remote_video <video> MediaElement
  peer_connection.onaddstream = function (event) {
    console.log("new remote stream added");
    connect_stream_to_src(event.stream, document.getElementById("remote_video"));
    // hide placeholder and show remote video
    document.getElementById("loading_state").style.display = "none";
    console.log("updating UI to open_call_state");
    document.getElementById("open_call_state").style.display = "block";
    $('#chat').removeAttr('hidden');
    $('#sendFileDiv').removeAttr('hidden');
    $('#chatAreaSend').removeAttr('disabled')
    $('#chatAreaSend').prop('placeholder',"Enter some text, then press Send.")
  };

  // setup stream from the local camera
  setup_video();

  // setup generic connection to the signaling server using the WebSocket API
  console.log("setting up connection to signaling server");
  signaling_server = new WebSocket("ws://localhost:1234");

  if (document.location.hash === "" || document.location.hash === undefined) { // you are the Caller
    console.log("you are the Caller");

    // create the unique token for this call
    var token = Date.now()+"-"+Math.round(Math.random()*10000);
    call_token = "#"+token;

    // set location.hash to the unique token for this call
    document.location.hash = token;

    signaling_server.onopen = function() {
      // setup caller signal handler
      signaling_server.onmessage = caller_signal_handler;

      // tell the signaling server you have joined the call
      console.log("sending 'join' signal for call token:"+call_token);
      signaling_server.send(
        JSON.stringify({
          token:call_token,
          type:"join",
        })
      );
    }

    document.title = "Caller";
    console.log("updating UI to loading_state");
    document.getElementById("loading_state").innerHTML = "Ready for a call...ask your friend to visit:<br/><br/>"+document.location;

  } else { // you have a hash fragment so you must be the Callee
    console.log("you are the Callee");

    // get the unique token for this call from location.hash
    call_token = document.location.hash;

    signaling_server.onopen = function() {
      // setup caller signal handler
      signaling_server.onmessage = callee_signal_handler;

      // tell the signaling server you have joined the call
      console.log("sending 'join' signal for call token:"+call_token);
      signaling_server.send(
        JSON.stringify({
          token:call_token,
          type:"join",
        })
      );

      // let the caller know you have arrived so they can start the call
      console.log("sending 'callee_arrived' signal for call token:"+call_token);
      signaling_server.send(
        JSON.stringify({
          token:call_token,
          type:"callee_arrived",
        })
      );
    }

    document.title = "Callee";
    console.log("updating UI to loading_state");
    document.getElementById("loading_state").innerHTML = "One moment please...connecting your call...";
  }

}

/* functions used above are defined below */

// handler to process new descriptions
function new_description_created(description) {
  peer_connection.setLocalDescription(
    description,
    function () {
      signaling_server.send(
        JSON.stringify({
          token:call_token,
          type:"new_description",
          sdp:description
        })
      );
    },
    log_error
  );
}

// handle signals as a caller
function caller_signal_handler(event) {
  var signal = JSON.parse(event.data);
  console.log(signal.type);
  if (signal.type === "callee_arrived") {
    create_offer();
  } else if (signal.type === "new_ice_candidate") {
    peer_connection.addIceCandidate(
      new rtc_ice_candidate(signal.candidate)
    );
  } else if (signal.type === "new_description") {
    peer_connection.setRemoteDescription(
      new rtc_session_description(signal.sdp),
      function () {
        if (peer_connection.remoteDescription.type == "answer") {
          // extend with your own custom answer handling here
        }
      },
      log_error
    );
  //////////SEÑAL PARA TRAER EL NOMBRE DEL ARCHIVO JUNTO AL SIZE//////
  } else if (signal.type === "file_signal") {
    fileReceived = {
      size: signal.fsize,
      name: signal.fname,
      type: signal.ftype
    }
  } else {
    // extend with your own signal types here
  }
}
function create_offer() {
  if (local_stream_added) {
    console.log("creating offer");
    peer_connection.createOffer(
      new_description_created,
      log_error
    );
  } else {
    console.log("local stream has not been added yet - delaying creating offer");
    setTimeout(function() {
      create_offer();
    }, 1000);
  }
}

// handle signals as a callee
function callee_signal_handler(event) {
  var signal = JSON.parse(event.data);
  console.log(signal.type);
  if (signal.type === "new_ice_candidate") {
    peer_connection.addIceCandidate(
      new rtc_ice_candidate(signal.candidate)
    );
  } else if (signal.type === "new_description") {
    peer_connection.setRemoteDescription(
      new rtc_session_description(signal.sdp),
      function () {
        if (peer_connection.remoteDescription.type == "offer") {
          create_answer();
        }
      },
      log_error
    );
  //////////SEÑAL PARA TRAER EL NOMBRE DEL ARCHIVO JUNTO AL SIZE//////
  } else if (signal.type === "file_signal") {
    fileReceived = {
      size: signal.fsize,
      name: signal.fname,
      type: signal.ftype
    }
  } else {
  // extend with your own signal types here
  }
}
function create_answer() {
  if (local_stream_added) {
    console.log("creating answer");
    peer_connection.createAnswer(new_description_created, log_error);
  } else {
    console.log("local stream has not been added yet - delaying creating answer");
    setTimeout(function() {
      create_answer();
    }, 1000);
  }
}

// setup stream from the local camera
function setup_video() {
  console.log("setting up local video stream");
  get_user_media(
    {
      "audio": true, // request access to local microphone
      "video": true  // request access to local camera
    },
    function (local_stream) { // success callback
      // display preview from the local camera & microphone using local <video> MediaElement
      console.log("new local stream added");
      connect_stream_to_src(local_stream, document.getElementById("local_video"));
      // mute local video to prevent feedback
      document.getElementById("local_video").muted = true;
      // add local camera stream to peer_connection ready to be sent to the remote peer
      console.log("local stream added to peer_connection to send to remote peer");
      peer_connection.addStream(local_stream);
      local_stream_added = true;
    },
    log_error
  );
}
// generic error handler
function log_error(error) {
  console.log(error);
}
